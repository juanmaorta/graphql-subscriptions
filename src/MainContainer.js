import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar'
import IconButton from '@material-ui/core/IconButton'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import WorldMap from './components/WorldMap'
import AirPortInfo from './components/AirPortInfo'
import HomeIcon from './util/HomeIcon'



class MainContainer extends Component {
  constructor (props) {
    super(props)

    this.state = {
      iataCode: null
    }
  }

  handleMarkerClick = (iataCode) => {
    console.log(`click ${iataCode}`)
    this.setState({
      iataCode
    })
  }


  render () {
    return (
      <div>
        <AppBar position='static'>
          <Toolbar>
            <IconButton aria-label='Home' href='/'>
              <HomeIcon />
            </IconButton>
            <Typography variant='h6' color='inherit'>SysLog POC</Typography>
            <AirPortInfo iataCode={this.state.iataCode} />
          </Toolbar>
        </AppBar>
        <WorldMap
          handleMarkerClick={this.handleMarkerClick}
        />
      </div>
    )
  }
}

export default MainContainer
