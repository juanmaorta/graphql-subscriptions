import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Query } from "react-apollo";
import gql from "graphql-tag";

const GET_AIRPORT_INFO = gql`
  query airport($iataCode: String)  {
     airportInfo(iataCode: $iataCode) {
       iataCode
       name
       location
       phone
     }
   }
`

class AirportInfo extends React.Component {
  render() {
    const { iataCode } = this.props

    return (
      <Query
        query={GET_AIRPORT_INFO}
        key={ iataCode }
        variables={{ iataCode }}
      >
        {({ loading, error, data }) => {
        if (loading) return null;
        if (error) return `Error!: ${error}`;

        return (
          <div style={{marginLeft: 400}}>
          { iataCode &&
            <div>
              <Typography variant='h5' color='black'>
                <div>{data.airportInfo.name} ({data.airportInfo.iataCode})</div>
              </Typography>
              <Typography variant='h6' color='inherit'>
                <div>{data.airportInfo.location}</div>
              </Typography>
            </div>
          }
          </div>
        );
      }}
      </Query>
    )
  }
}

export default AirportInfo
