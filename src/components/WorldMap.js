import React, { Component } from 'react';
import {
  GeoJSON,
  Map as LeafletMap,
  Marker
} from 'react-leaflet';
import worldGeoJSON from 'geojson-world-map';
import { Query } from "react-apollo";

import {
  ATTRIBUTION_CONTROL,
  BASE_ZOOM,
  CENTER_COORDS,
  DISPLAY_ZOOM_CONTROL,
  DOUBLE_CLICK_ZOOM,
  IS_MAP_DRAGABBLE,
  MAX_ZOOM,
  SCROLL_WHEEL_ZOOM
} from '../config/constants'

import gql from "graphql-tag";

const GET_AIRPORT_LIST = gql`
  query airports {
    airports {
      total
      airports {
        iataCode
				coords
      }
    }
  }
`

class WorldMap extends Component {
  render() {
      return (
        <Query
          query={GET_AIRPORT_LIST}
        >
        {({ loading, error, data }) => {
          if (loading) return null;
          if (error) return `Error!: ${error}`;

          return (
            <LeafletMap
              center={CENTER_COORDS}
              zoom={BASE_ZOOM}
              maxZoom={MAX_ZOOM}
              attributionControl={ATTRIBUTION_CONTROL}
              zoomControl={DISPLAY_ZOOM_CONTROL}
              doubleClickZoom={DOUBLE_CLICK_ZOOM}
              scrollWheelZoom={SCROLL_WHEEL_ZOOM}
              dragging={IS_MAP_DRAGABBLE}
              animate
              easeLinearity={0.35}
            >
              <GeoJSON
                data={worldGeoJSON}
                style={() => ({
                  color: "#000",
                  weight: 1,
                  fillColor: "#00ff00",
                  fillOpacity: 0.5
                })}
              />
              {data.airports.airports.map((airport, idx) => {
                return (
                  <Marker
                    position={airport.coords}
                    key={idx}
                    onClick={() => this.props.handleMarkerClick(`${airport.iataCode}`)}
                  />
                )
              })}
            </LeafletMap>
            )
          }}
        </Query>
      );
    }
}

export default WorldMap
