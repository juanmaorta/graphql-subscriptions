import rp from 'request-promise';
import fs from 'fs';
import { sampleSize } from 'lodash/collection';

const AIPORT_INFO_URI = 'https://airport-info.p.rapidapi.com/airport';
let airportSamples = []

/**
 * @TODO
 * this function has a beautiful side effect!!!
*/
export const addAirport = () => {
  const airport = {
    iataCode: 'ABC',
    name: 'Some random airport name',
    location: null,
    phone: null,
    coords: [0,0]
  }
  airportSamples.push(airport)
  console.log(airportSamples.length)
  return airport
}

export const getAirportInfo = (iataCode) => {
  const options = {
    uri: AIPORT_INFO_URI,
    qs: {
      'iata': iataCode
    },
    headers: {
      'X-RapidAPI-Key': 'ab8d0b8533msh70888f41006e5a5p15561cjsnaf5fff95c94e'
    }
  }

  return rp(options)
    .then((res) => {
      const data = JSON.parse(res)

      return {
          iataCode: data.iata,
          location: data.location,
          name: data.name,
          phone: data.phone
      };
    })
    .catch((err) => {
      return err;
    })
}

/**
 * @TODO
 * this function has a beautiful side effect!!!
*/
export const getAirportList = () => {
  const airports = []
  const SAMPLE_SIZE = 10

  var content = fs.readFileSync(process.cwd() + "/src/server/airports_full.json");
  var airportObj = JSON.parse(content);

  airportObj.map((airport) => {
    airports.push({
      iataCode: airport.code,
      coords: [
        airport.lat,
        airport.lon
      ]
    })

    return null
  })

  airportSamples = sampleSize(airports, SAMPLE_SIZE)

  return {
    total: SAMPLE_SIZE,
    airports: airportSamples
  }
}
