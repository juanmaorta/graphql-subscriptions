import { makeExecutableSchema }  from 'graphql-tools';

import { resolvers } from './resolvers';

// Construct a schema, using GraphQL schema language
const typeDefs = `
  type Airport {
    iataCode: String,
    name: String,
    location: String,
    phone: String,
    coords: [Float]
  }

  type AirportList {
    total: Int,
    airports: [Airport]
  }

  type Query {
    hello: String,
    fireMessage: Airport,
    airportInfo(iataCode: String): Airport,
    airports: AirportList
  }

  type Subscription {
    airportsRealTime: AirportList
  }
`

const schema = makeExecutableSchema({typeDefs, resolvers});

export { schema };
