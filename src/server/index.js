import express from 'express';
import { execute, subscribe } from 'graphql';
import { createServer } from 'http';
import { SubscriptionServer } from 'subscriptions-transport-ws';

import {
  graphqlExpress,
  graphiqlExpress,
} from 'apollo-server-express';
import bodyParser from 'body-parser';
import cors from 'cors';

import { schema } from './schema';

const PORT = 4000;
const GRAPHQL_URL = "/graphql";


const server = express();

// Endpoint to check if the API is running
server.get('/api/status', (req, res) => {
 res.send({ status: 'ok' });
});

server.use(cors({
 origin: '*', // Be sure to switch to your production domain
 optionsSuccessStatus: 200
}));

server.use(GRAPHQL_URL, bodyParser.json(), graphqlExpress({
  schema: schema
}));

server.use('/graphiql', graphiqlExpress({
  endpointURL: GRAPHQL_URL
}));

// Wrap the Express server
const ws = createServer(server)

ws.listen(PORT, () => {
  console.log(`Running a GraphQL API server at localhost:${PORT}${GRAPHQL_URL}`);
  // Set up the WebSocket for handling GraphQL subscriptions
  new SubscriptionServer({
    execute,
    subscribe,
    schema
  }, {
    server: ws,
    path: '/subscriptions',
  });
});
