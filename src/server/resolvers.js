import { PubSub } from 'graphql-subscriptions';

import * as resolver_functions from './resolver_functions';

const pubsub = new PubSub();

export const resolvers = {
  Query: {
    hello: () => {
      return 'Hello world!';
    },
    fireMessage: () => {
      const airport = resolver_functions.addAirport()

      pubsub.publish('airportsAdded', { airport });
      return airport
    },
    airportInfo: async (root, { iataCode }) => {
      return await resolver_functions.getAirportInfo(iataCode)
    },
    airports: () => {
      return resolver_functions.getAirportList();
    }
  },

  Subscription: {
    airportsRealTime: async (root, { iataCode }) => {
      return resolver_functions.getAirportList();
    },
  }
}
