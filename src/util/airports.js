const airports = [
  {
    iataCode: 'AAE',
    name: 'El Mellah Airport',
    coords: [36.8236, 7.8103]
  },
  {
    iataCode: 'AAL',
    name: 'Aalborg Airport',
    coords: [57.0952, 9.85606]
  },
  {
    iataCode: 'BCN',
    name: 'Barcelona International Airport',
    coords: [41.3006, 2.07976]
  },
  {
    iataCode: 'DEL',
    name: 'Indira Gandhi International Airport',
    coords: [28.5603, 77.1027]
  },
  {
    iataCode: 'WNN',
    name: 'Wunnummin Lake Airport',
    coords: [52.9228, -89.1934]
  },
  {
    iataCode: 'LHR',
    name: 'London Heathrow Airport',
    coords: [51.4703, -0.45342]
  },
]

export default airports;
