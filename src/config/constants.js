export const IS_MAP_DRAGABBLE = true;
export const BASE_ZOOM = 3;
export const MAX_ZOOM = 10;
export const CENTER_COORDS = [50, 10];
export const DISPLAY_ZOOM_CONTROL = true;
export const DOUBLE_CLICK_ZOOM = true;
export const SCROLL_WHEEL_ZOOM = true;
export const ATTRIBUTION_CONTROL = false;

export const AIRPORT_API_URI = "http://localhost:4000/graphql";
