This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## Install dependencies
### `yarn`


## Available Scripts

In the project directory, you can run:

### `yarn start`
Runs the app in the development mode (client side).<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


### `yarn start:server`
Runs the GraphQL server
Open [http://localhost:4000/graphiql](http://localhost:4000/graphiql) to view the GraphQL query interface in the browser
